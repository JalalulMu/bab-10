#include <iostream>
using namespace std;

void prosesEsok(int a);

int main()
{
	int a, esok;
	
	cout<<"Masukan Hari Dengan Nomor Pada data !"<<endl;
	cout<<"|1. Senin\t|"<<endl;
	cout<<"|2. Selasa\t|"<<endl;
	cout<<"|3. Rabu\t|"<<endl;
	cout<<"|4. Kamis\t|"<<endl;
	cout<<"|5. Jum'at'\t|"<<endl;
	cout<<"|6. Sabtu\t|"<<endl;
	cout<<"|7. Minggu\t|"<<endl<<endl;
	cout<<"Masukan \t : ";cin>>a;
	cout<<endl;
	prosesEsok(a);
	
	return 0;
}

void prosesEsok(int a)
{
	switch (a)
	{
		case 1 :
			cout<<"Hari ini hari Senin, besok adalah hari Selasa"<<endl;
			break;
		case 2 :
			cout<<"Hari ini hari Selasa, besok adalah hari Rabu"<<endl;
			break;
		case 3 :
			cout<<"Hari ini hari Rabu, besok adalah hari Kamis"<<endl;
			break;
		case 4 :
			cout<<"Hari ini hari Kamis, besok adalah hari Jum'at'"<<endl;
			break;
		case 5 :
			cout<<"Hari ini hari Jum'at, besok adalah hari Sabtu"<<endl;
			break;
		case 6 :
			cout<<"Hari ini hari Sabtu, besok adalah hari Minggu"<<endl;
			break;
		case 7 :
			cout<<"Hari ini hari Minggu, besok adalah hari Senin"<<endl;
			break;
		default :
			cout<<"Eror"<<endl;
			break;
	}
}
